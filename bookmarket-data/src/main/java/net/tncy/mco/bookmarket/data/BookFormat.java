package net.tncy.mco.bookmarket.data;

public enum BookFormat {
    BROCHE, POCHE
}
