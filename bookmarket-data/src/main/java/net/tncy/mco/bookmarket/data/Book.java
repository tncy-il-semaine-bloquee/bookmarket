package net.tncy.mco.bookmarket.data;

import net.tncy.mco.validator.ISBN;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String title;

    @NotNull
    private String author;

    @NotNull
    private String publisher;

    @NotNull
    private BookFormat format;

    @NotNull
    @ISBN
    private String isbn;

    public Book() {

    }

    public Book(@NotNull String title, @NotNull String author, @NotNull String publisher, @NotNull BookFormat format, @NotNull String isbn) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.isbn = isbn;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public BookFormat getFormat() {
        return format;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
