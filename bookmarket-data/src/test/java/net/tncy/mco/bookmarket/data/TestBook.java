package net.tncy.mco.bookmarket.data;

import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestBook
{

    @Test
    public void testBookCreationSuccess() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Book b = new Book("Titre", "Auteur", "Publication", BookFormat.POCHE, "1234536783");
        Set<ConstraintViolation<Book>> violations = validator.validate(b);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testBookCreationFail() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Book b = new Book("Titre", "Auteur", "Publication", BookFormat.POCHE, "difhhor");
        Set<ConstraintViolation<Book>>  violations = validator.validate(b);
        assertFalse(violations.isEmpty());
    }
}
